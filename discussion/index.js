// Repetition Control Structures
// Loops - lets us execute code repeatedly in a pre-set number of the time or maybe forever(infinite loop)

// While loop
// It takes in an expression/condition before proceeding in the evaluation of the codes.
/* Syntax:
	while(expression/condition){
		statement/s;
	}


*/
let count = 5;
while(count !== 0){
	console.log('While loop:' + count);

	count --;
} //output is 5 4 3 2 1 
// Mini- activity loop numbers from 1-10.

let count1 = 1;
while(count1 <= 10){
	console.log(count1);
	
	count1++;
} //ouput is 1 2 3 4 5 6 7 8 9 10

// Do-While loop
// at least one code block will be executed before proceeding to the condition.
/*Syntax:
	do{
		statement/s;
	} while(expression/condition)
*/

let countThree = 5;

do{
	console.log('Do-While loop: '+ countThree)

	countThree--;
}while(countThree > 0)


let count2 = 1;

do{
	console.log(count2);

	count2++;
}while(count2 <=10 )

// For loop - more flexible looping construct
/*
	for(initialization; expression/condition; finalExpression){
		statement/s;
	}

*/ 

for(let countFive = 5; countFive > 0; countFive--){
	console.log('For loop: '+ countFive);
}

/*let number = Number(prompt('Give me a number'));

for(let numCount = 1; numCount <= number; numCount++){
		console.log('Hello Batch 144');
}
*/
let myString = 'alex';
// console.log(myString.length);

// console.log(myString[2]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}
// myString.length = 4
// x = 0 1 2 3

// Array
// element - represents the values in the array
// index - location of values(elements) in the array which starts at index 0.
// elements - a l e x
//  index -   0 1 2 3

let myName = 'Alex';

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){
		//if the letter in the name is a vowel, it wil print 3
		console.log(3);
	}else{
		// Will print in the console if character is a non-vowel
		console.log(myName[i]);
	}
}

/* Continue and Break statements
	Continue - allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	Break - used to terminate the current loop once a match has been found
*/

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0
	if(count % 2 === 0){
		// tells the code to continue to the enxt iteration of the loop
		continue;
	}
	// the current value of number is printed out if the remainder is not equal to 0
	console.log('Continue and Break:' + count);

	// If the current value of count is greater than 10
	if(count > 10){

		// tells to terminate the loop even if the loop condition is still being satisfied.
		break
	}
}
// count: 9 10 11

let name = 'Alexandro'; 

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase()=== 'a'){
		console.log('Continue to the next iteration');
		continue;
	}
	if(name[i].toLowerCase() === 'd'){
		break;
	}
}